<?php
include_once("connection/config.php");

if(isset($_POST['prodstatus'])) {
    $prodstatus = $_POST['prodstatus'];
    if($prodstatus == "1")
    {
            $query = "SELECT p_i.img_name, p.p_id, p.p_name, p.orderId, p.p_code, p.price, 
            p.quantity, p.order, p.p_status, p.createdat, p.updatedat, c.Name
            FROM products p
            INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
            LEFT JOIN category c ON c.orderId = p.orderId
            WHERE p_i.I_status = '1'
            AND c.status = '1'
            AND p.p_status = '1'";
    } 
    elseif($prodstatus == "0")
    {
        $query = "SELECT p_i.img_name, p.p_id, p.p_name, p.orderId, p.p_code, p.price, p.quantity, p.order, p.p_status, p.createdat, p.updatedat, c.Name
        FROM products p
        INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
        LEFT JOIN category c ON c.orderId = p.orderId
        WHERE p_i.I_status = '1'
        AND c.status = '1'
        AND p.p_status = '0'";
    } else
    {
        $query = "SELECT p_i.img_name, p.p_id, p.p_name, p.orderId, p.p_code, p.price, p.quantity, p.order, p.p_status, p.createdat, p.updatedat, c.Name
        FROM products p
        INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
        LEFT JOIN category c ON c.orderId = p.orderId
        WHERE p_i.I_status = '1'
        AND c.status = '1'";

    }

}elseif (isset($_POST['minPrice']) && isset($_POST['maxPrice'])){
    $minPrice = $_POST['minPrice'];
    $maxPrice = $_POST['maxPrice'];

    $query = "SELECT p_i.img_name, p.p_id, p.p_name, p.orderId, p.p_code, p.price, p.quantity, p.order, p.p_status, p.createdat, p.updatedat, c.Name
    FROM products p
    INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
    LEFT JOIN category c ON c.orderId = p.orderId
    WHERE p_i.I_status = '1'
    AND c.status = '1'
    AND price BETWEEN '$minPrice' AND '$maxPrice'";
    
}elseif(isset($_POST['minquantity']) && isset($_POST['maxquantity'])){
    $minquantity = $_POST['minquantity'];
    $maxquantity = $_POST['maxquantity'];

    $query = "SELECT p_i.img_name, p.p_id, p.p_name, p.orderId, p.p_code, p.price, p.quantity, p.order, p.p_status, p.createdat, p.updatedat, c.Name
    FROM products p
    INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
    LEFT JOIN category c ON c.orderId = p.orderId
    WHERE p_i.I_status = '1'
    AND c.status = '1'
    AND quantity BETWEEN '$minquantity' AND '$maxquantity'";


}else
{

    // if(isset($_POST['product_search'])){
    $searchproduct = $_POST['product_search'];
    // echo $searchproduct;

    if($searchproduct == ''){

    $query = "SELECT p_i.img_name,p.p_id,p.p_name,p.orderId,p.p_code,p.price,p.quantity,p.order,p.p_status,p.createdat,p.updatedat,c.Name
    FROM products p
    INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
    LEFT JOIN category c ON c.orderId = p.orderId
    WHERE p_i.I_status = '1'
    AND c.status = '1'";

    }else{
    $query = "SELECT p_i.img_name,p.p_id,p.p_name,p.orderId,p.p_code,p.price,p.quantity,p.order,p.p_status,p.createdat,p.updatedat,c.Name
    FROM products p
    INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
    LEFT JOIN category c ON c.orderId = p.orderId
    WHERE p_i.I_status = '1'
    AND c.status = '1' AND `p_name` LIKE '%$searchproduct%' ";
    }
}

$path = "img/";
$row = mysqli_query($link, $query);

if (mysqli_num_rows($row) > 0)  {
       
    foreach ($row as $key => $value) {

        echo "<tr>";
        echo "<td>" . $value['p_id'] . "</td>";
        echo "<td >" . $value['p_name'] . "</td>";
        echo "<td>" . $value['p_code'] . "</td>";
        echo "<td>".$value['Name']."</td>";
        ?>
        <td><img src="img/<?php echo $value['img_name'] ?>" width="100px" height="100px" alt="notdisplay"></td>
        <?php
        echo "<td>".$value['price']."</td>";
        echo "<td>".$value['quantity']."</td>";
        echo "<td>".$value['order']."</td>";
        ?>
        <td><?php if ($value['p_status'] == 1) {
        echo '<button class="btn btn-success" onclick = ActiveInActive(' . $value["p_id"] . '); >Active</button>';
        } else {
        echo '<button class="btn btn-danger" onclick = ActiveInActive(' . $value["p_id"] . ');>InActive</button>';
        } ?>
        </td>
        <?php
         echo "<td>" . $value['createdat'] . "</td>";
         echo "<td>" . $value['updatedat'] . "</td>";
         ?>
         <td>
        <?php
        echo "<a href='Edit_product.php?id=" . $value['p_id'] . "' title='Update Record' data-toggle='tooltip' ><span class='glyphicon glyphicon-pencil'></span></a>";     
        ?>
        <a class=""  title='Delete Record' data-toggle='tooltip' id="btndelete" onclick="product_delete(<?php echo $value['p_id']; ?>)" ><span class='glyphicon glyphicon-trash'></span></a>
        </td>
        <?php
        }
}else{
    echo "<script>";
    echo "alert('No any record found')";
    echo "</script>";
    // exit();
    }


?>