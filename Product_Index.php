<?php
session_start();
include_once("connection/config.php");
// include_once("header.php");

?>
<!DOCTYPE html>
<html>

<head>
    <title>Product Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <script src="js/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"></script>
    <script src="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css"></script> -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>  -->
    <link href="../project1/assets/css/nav.css" rel="stylesheet" type="text/css" media="all" />
    <script src=" https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src=" https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src=" https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css"  rel="stylesheet"> -->
</head>
<style type="text/css">
    .topnav a:hover {
        background-color: #4CAF50;
        color: white;
    }
</style>

<body>
    <div class="topnav" id="myTopnav">
        <a href="../project1/index.php" class="">Home </a>
        <a href="../project1/Add_category.php">Add Category</a>
        <a href="../project1/Add_product.php">Add Product</a>
        <a href="../project1/index.php">View Category</a>
        <a href="../project1/Product_Index.php">View Product</a>
        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
            <a href="logout.php" style="float: right;">Logout </a>
            <h4 style="color: white;float:right;padding:5px;">Welcome <?php echo $_SESSION['email']; ?></h4><br>
            <i class="fa fa-bars"></i>
        </a>
    </div>
    <div class="col">
        <select class="" style="float:right; padding-top:30px;" name="prodstatus" id="prodstatus">
            <option selected>all</option>
            <option value="1">Active</option>
            <option value="0">Inactive</option>
        </select>
    </div>
    <div class="form-group" style="width: 25%;">
        <label for="search">Search :</label>
        <input type="text" class="form-control" id="search_product" name="search_product" autofocus placeholder="Search">

    </div>
    <div class="form-row">
        <div class="form-group " style="width: 10%;">
            <input type="text" class="form-control" id="minValueprice" name="minValueprice" placeholder="Enter min price">
        </div>
        <div class="form-group ml-5" style="width: 10%;">
            <input type="text" class="form-control" id="maxValuePrice" name="maxValuePrice" placeholder="Enter Max price">
        </div>
        <div class="form-group ml-3" style="width: 10%;">
            <button type="submit" name="minmax" id="minmax" class="btn btn-success mb-2">Filter Price</button>
        </div>
    <!-- </div>
    <div class="form-row"> -->
        <div class="form-group" style="width: 10%;">
            <input type="text" class="form-control" id="minValuequantity" name="minValuequantity" placeholder="Enter min quantity">
        </div>
        <div class="form-group ml-5" style="width: 10%;">
            <input type="text" class="form-control" id="maxValuequantity" name="maxValuequantity" placeholder="Enter Max quantity">
        </div>
        <div class="form-group ml-3" style="width: 25%;">
            <button type="submit" name="minmax" id="minmaxquantity" class="btn btn-success mb-2">Filter Quantity</button>
            <button type="submit" name="resetAll" id="resetAll" class="btn btn-danger mb-2">reset</button>
        </div>
    </div>

    </div>

    </div>
    <table class="table  table-bordered table-striped" id="example">
        <thead>
            <tr>
                <th>Product ID</th>
                <th>Product Name</th>
                <th>Product Code</th>
                <th>Category Name</th>
                <th>Product Img</th>
                <th>Product Price</th>
                <th>Product Qty</th>
                <th>Product Order</th>
                <th>Product status</th>
                <th>CreatedAt</th>
                <th>UpdatedAt</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="data_table">
            <?php
            $query = "SELECT p_i.img_name,p.p_id,p.p_name,p.orderId,p.p_code,p.price,p.quantity,p.order,p.p_status,p.createdat,p.updatedat,c.Name
                FROM products p
                INNER JOIN  p_image p_i ON p.p_id = p_i.p_id 
                LEFT JOIN category c ON c.orderId = p.orderId
                WHERE p_i.I_status = '1'
                AND c.status = '1'";


            $res = mysqli_query($link, $query);
            if (mysqli_num_rows($res) > 0) {

                while ($row = mysqli_fetch_assoc($res)) {
            ?>
                    <tr>
                        <td><?php echo $row['p_id']; ?></td>
                        <td><?php echo $row['p_name']; ?></td>
                        <td><?php echo $row['p_code']; ?></td>
                        <td><?php echo $row['Name']; ?></td>
                        <td><img src="img/<?php echo $row['img_name'] ?>" width="100px" height="100px" alt="notdisplay"></td>
                        <td><?php echo $row['price']; ?></td>
                        <td><?php echo $row['quantity']; ?></td>
                        <td><?php echo $row['order']; ?></td>
                        <td><?php if ($row['p_status'] == 1) {
                                echo '<button class="btn btn-success" onclick = ActiveInActive(' . $row["p_id"] . '); >Active</button>';
                            } else {
                                echo '<button class="btn btn-danger" onclick = ActiveInActive(' . $row["p_id"] . ');>InActive</button>';
                            } ?></td>
                        <td><?php echo $row['createdat']; ?></td>
                        <td><?php echo $row['updatedat']; ?></td>
                        <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->

                        <td><?php  

                            echo "<a href='Edit_product.php?id=" . $row['p_id'] . "' title='Update Record' data-toggle='tooltip' ><span class='glyphicon glyphicon-pencil'></span></a>";
                            ?>
                            <a class="" title='Delete Record' data-toggle='tooltip' id="btndelete" onclick="product_delete(<?php echo $row['p_id']; ?>)"><span class='glyphicon glyphicon-trash'></span></a>
                        </td>
                <?php
                }
            }
                ?>
        </tbody>
    </table>
</body>
<script src="js/validation.js"></script>
<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>
<script src="js/validation.js"></script>
<script src="js/sweetalert.min.js"></script>


<script>
    $(document).ready(function() {

        $("#minmax").click(function() {

            let minPrice = $("#minValueprice").val();
            let maxPrice = $("#maxValuePrice").val();
            // trim(minPrice);
            // trim(maxPrice);
            // console.log(minPrice);
            if (minPrice != "" && maxPrice != "") {
                // debugger;
                $.ajax({
                    url: "Product_search.php",
                    method: "POST",
                    data: {
                        minPrice: minPrice,
                        maxPrice: maxPrice
                    },
                    success: function(data) {
                        $("tbody").html(data);
                    }
                });

            } else {
                alert('Please Enter Min and max Price');
            }
        });

        $("#minmaxquantity").click(function() {

            let minquantity = $("#minValuequantity").val();
            let maxquantity = $("#maxValuequantity").val();

            if (minquantity != "" && maxquantity != "") {
                // debugger;
                $.ajax({
                    url: "Product_search.php",
                    method: "POST",
                    data: {
                        minquantity: minquantity,
                        maxquantity: maxquantity
                    },
                    success: function(data) {
                        $("tbody").html(data);
                    }
                });

            } else {
                alert('Please Enter Min and max quantity');
            }
        });

        $("#resetAll").click(function() {
            $("#minValuequantity").val("");
            $("#maxValuequantity").val("");
            $("#minValueprice").val("");
            $("#maxValuePrice").val("");
            // $("#searchpro").val("");
            $.ajax({
                url: "p_search.php",
                method: "POST",
                data: {},
                success: function(data) {
                    $("tbody").html(data);
                }
            });
        });
    });

    function ActiveInActive(id) {
        var id = id;
        $.ajax({
            type: "POST",
            url: "Product_status.php",
            data: {
                id: id
            },
            success: function(value) {
                $("#data_table").html(value);
            }

        });

    }

    function product_delete(del) {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    var id = del;
                    $.ajax({
                        type: "POST",
                        url: "Delete_Product.php",
                        data: {
                            id: id
                        },
                        success: function(value) {
                            $("#data_table").html(value);
                        }
                    });
                    swal("Your record has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Your record is not deleted!", {
                        icon: "error",
                    });
                }
            })
    }
    $(document).ready(function() {
            $('#example').DataTable({
                "aoColumnDefs": [{
                    
                    "bSortable": false,
                    "aTargets": [0,1,2,3,4,7,8,9,10,11]
                }]
            });
        });
</script>