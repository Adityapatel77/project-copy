$(document).ready(function () {

    $('#status').change(function(){
        var stat = $('#status').val();
        $.ajax({
            url:"search.php",
            method:"POST",
            data:{
                stat : stat
            },
            success:function(data){
                $("tbody").html(data); 
            }
        });
    });
    
    $('#prodstatus').change(function(){
        var prodstatus = $('#prodstatus').val();
        $.ajax({
            url:"product_search.php",
            method:"POST",
            data:{
                prodstatus : prodstatus
            },
            
            success:function(data){
                $("tbody").html(data); 
            }
        });
    });


    var wage = document.getElementById("searchcat");
    wage.addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            //checks whether the pressed key is "Enter"
            searchcat();
        }
    });

});


function searchcat() {
    var searchcat = $('#searchcat').val();
    $.ajax({
        url: "search.php",
        method: "POST",
        data: {
            searchcat: searchcat
        },
        success: function (data) {
            $("tbody").html(data);
        }
    });
}

$(document).ready(function () {

    var wage = document.getElementById("search_product");
    wage.addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            //checks whether the pressed key is "Enter"
            product_search();
        }
    });

});


function product_search() {
var product_search = $('#search_product').val();
    $.ajax({
        url: "Product_search.php",
        method: "POST",
        data: {
            product_search: product_search
        },
        success: function (data) {
        $("tbody").html(data);
        }
    });
}

