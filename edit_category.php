<?php
require_once("connection/config.php");
?>
<?php  
session_start();  
  
if(!$_SESSION['email'])  
{  
  
    header("Location: login.php");//redirect to the login page to secure the welcome page without login access.  
}  
  
?> 

<!DOCTYPE html>
<html>
<head>
	<title>Edit Category</title>
<!-- 	 <link rel = "icon" href = "../project/image/img_tree.png" type = "image/x-icon"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="js/jquery.min.js"></script>
    <style type="text/css">
        .wrapper{
            width: 100%;
            margin: 0 auto;
            
        }
        .form-group{
        	width : 50%;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Edit Category</h2>
                        <?php
                          $orderid = isset($_GET['id']) ? $_GET['id'] : '';
                          $query = "SELECT * FROM `category` Where `orderId` = '{$orderid}'";
                          $result = mysqli_query($link,$query) or die(mysqli_error($link));
                          if(mysqli_num_rows($result) > 0){
                          while($row = mysqli_fetch_assoc($result)){

                        ?>
                    </div>
                     <form action="update_category.php" enctype="multipart/form-data" method="post">
                        <div class="form-group ">
                            <input type="hidden" name="id" value="<?php echo $orderid ?>"/>
                            <!-- <?php echo $orderid;?> -->
                            <label>Name</label>
                            <input type="text" name="edit_name" id="name" class="form-control" required value="<?php echo $row['Name'] ?>">
                            <div id="name_response"></div>
                        </div>           
                         <div class="form-group ">
                            <label>Order</label>
                            <input type="text" name="edit_order" class="form-control" value="<?php echo $row['order'] ?>"  required>
                            
                        </div>
                       
                        <div class="form-group">
                        	<label>Status :</label>
                            <?php $radio= $row['status']; ?> 
                            <input type="radio" id="active" name="edit_status" value=1 <?php echo ($radio== 1) ?  "checked" : " " ;  ?>/>
							<label for="Active">Active</label>
							<input type="radio" id="inactive" name="edit_status" value=0 <?php echo ($radio== 0) ?  "checked" : " " ;  ?>/>
							<label for="inactive">Inactive</label><br>
                        </div>
                      
                         <div class="form-group ">
                        	<label>Image</label><br>
                            <img src="images/<?php echo $row['image']?>" width="100px" height="100px"><br><br>
                            <?php echo $row['image']; ?>
                        	<input class="form-control form-control-lg" name="edit_image" type="file"   required />
                        

                        </div>
                       

                        <input type="submit" name="edit" class="btn btn-primary" value="Submit">
                        <a href="index.php"   class="btn btn-default">Cancel</a>
                    </form>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
// $(document).ready(function(){

// $("#name").keyup(function(){

//    var Name = $(this).val();

//    if(Name != ''){

//       $.ajax({
//          url: 'checkname.php',
//          type: 'post',
//          data: {Name: Name},
//          success: function(response){

//              $('#name_response').html(response);

//           }
//       });
//    }else{
//       $("#name_response").html("");
//    }

//  });

// });
</script>