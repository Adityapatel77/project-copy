<?php  
session_start();  
  
if(!$_SESSION['email'])  
{  
  
    header("Location: login.php");//redirect to the login page to secure the welcome page without login access.  
}  
  
?> 
<!DOCTYPE html>
<html>
<head>
	<title>Add Category</title>
<!-- 	 <link rel = "icon" href = "../project/image/img_tree.png" type = "image/x-icon"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="js/jquery.min.js"></script>

    <style type="text/css">
        .wrapper{
            width: 100%;
            margin: 0 auto;
            
        }
        .form-group{
        	width : 50%;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Category</h2>
                    </div> 
                     <form action="storecategory.php" enctype="multipart/form-data" method="post">
                        <div class="form-group ">
                            <label>Name</label>
                            <input type="text" name="Name" id="name" class="form-control" required>
                            <div id="name_response"></div>
                        </div>           
                         <div class="form-group ">
                            <label>Order</label>
                            <input type="text" name="order" class="form-control" value = "<?php echo time()+mt_rand(10,5000);?>" disabled >
                        </div>
                       
                        <div class="form-group">
                        	<label>Status :</label>
                        	<input type="radio" id="active" name="status" value="1">
							<label for="Active">Active</label>
							<input type="radio" id="inactive" name="status" value="0">
							<label for="inactive">Inactive</label><br>
                        </div>
                      
                         <div class="form-group ">
                        	<label>Image</label>
                        	<input class="form-control form-control-lg" name="image" type="file" required />

                        </div>
                       

                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<script>
$(document).ready(function(){
// $( "#submit" ).prop( "disabled", true );
$("#name").keyup(function(){

   var Name = $(this).val();

   if(Name != ''){

      $.ajax({
         url: 'checkname.php',
         type: 'post',
         data: {Name: Name},
         success: function(response){
            
             $('#name_response').html(response);
            //  $( "#submit" ).prop( "disabled", false ); //enable it back

          }
      });
   }else{
      $("#name_response").html("");
      
   }

 });

});
</script>
<script></script>