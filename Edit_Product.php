<?php
require_once("connection/config.php");
?>
<?php
session_start();
$id = $_GET['id'];
$_SESSION["id"] = $id;

if (!$_SESSION['email']) {

    header("Location: login.php"); //redirect to the login page to secure the welcome page without login access.  
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Edit Product</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>

    <!-- 	 <link rel = "icon" href = "../project/image/img_tree.png" type = "image/x-icon"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper {
            width: 100%;
            margin: 0 auto;

        }

        .form-group {
            width: 50%;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Edit Product</h2>
                        <?php
                        $Proid = isset($_GET['id']) ? $_GET['id'] : '';
                        //   echo $Proid;
                        $query = "SELECT * FROM `products` Where `p_id` = '{$Proid}'";
                        $result = mysqli_query($link, $query) or die(mysqli_error($link));
                        if (mysqli_num_rows($result) > 0) {
                            while ($row = mysqli_fetch_assoc($result)) {
                        ?>

                    </div>
                    <form action="update_product.php" id="edit_form" enctype="multipart/form-data" method="post">
                        <div class="form-group ">
                            <label>Name</label>
                            <input type="text" name="p_name" class="form-control" value="<?php echo $row['p_name'] ?>" required>

                        </div>
                        <div class="form-group" >
                            <label for="category"> category </label>
                            <?php
                                include_once("connection/config.php");
                                $sql = "SELECT `orderId`, `Name` FROM category WHERE `status` = '1'";
                                $res = mysqli_query($link, $sql);
                                echo '<select class="form-control form-rounded" style="height:34px;" name="catName">';
                                foreach ($res as $key => $value) {
                                    echo '<option value = ' . $value["orderId"] . '> ' . $value["Name"] . ' </option>';
                                }
                                echo '</select>';
                            ?>
                        </div>
                        <div class="form-group ">
                            <label>Product Code</label>
                            <input type="text" name="p_code" class="form-control" value="<?php echo time() + mt_rand(10, 5000); ?>" disabled>
                        </div>

                        <div class="form-group ">
                            <label>Price</label>
                            <input type="text" name="price" class="form-control" value="<?php echo $row['price'] ?>">
                        </div>
                        <div class="form-group ">
                            <label>Quantity</label>
                            <input type="text" name="quantity" class="form-control" value="<?php echo $row['quantity'] ?>">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Order</label>
                            <select class="form-control" style="height:34px;" name="order" id="sel1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>
                        <?php
                                $query1 = "SELECT pi.I_id,pi.p_id,pi.I_status,pi.img_name FROM p_image pi WHERE p_id = $Proid";
                                $req = mysqli_query($link, $query1) or die(mysqli_error($link));
                                if ($req) {
                                    foreach ($req as $key => $value1) {
                                        if ($value1['I_status'] == 1) {
                                            echo '<img class="form-rounded" style="width:200px; height:200px;  border:5px solid green;  margin:10px " src="img/' . $value1["img_name"] . '">';
                                        } else {
                                            echo "<div>";
                                            // echo "</br>";
                                            echo '<img style="width:200px; height:200px;  margin-bottom:0px;" src="img/' . $value1["img_name"] . '">';

                                            echo "<a style='margin:10px;' href='Image_Update.php?imgUpdateId=" . $value1['I_id'] . "&imgstatus=2' class='btn btn-success form-rounded' > Active </a> ";
                                            echo "<a href='Image_Update.php?imgDeleteId=" . $value1['I_id'] . "&imgstatus=5' class='btn btn-danger form-rounded' > Delete </a> ";
                                            echo "</div>";
                                            echo "</br>";
                                        }
                                    }
                                }
                                ?>
                                </br>
                                    <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                                    <a href="Product_index.php" class="btn btn-default">Cancel</a>
                                </form>
                     <?php
                            } 
                                } else {
                                    echo "<script>";
                                    echo "alert('No any record for this id')";
                                    echo "</script>";
                                }
                     ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<!-- <script>
$("#edit_form").submit(function(e) {
    e.preventDefault();
});

</script> -->