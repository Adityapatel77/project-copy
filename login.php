<?php
include_once("connection/config.php");
session_start();
if(isset($_POST['login'])){
    $email = $_POST['email'];
    $pass = $_POST['password'];

    $check_user = "SELECT * FROM `login` WHERE `email` = '$email' && `password` = '$pass'";
    $sql = mysqli_query($link,$check_user);

    if(mysqli_num_rows($sql)){
        $_SESSION['email'] = $email;
        header('Location:index.php');
    }else{
        echo "<script>alert('Email or password is incorrect!')</script>"; 
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Sign Up</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">

<link rel="stylesheet" href="../project1/assets/style.css">
</head>
<body>
<div class="main">

<section class="signup">
<div class="container">
<div class="signup-content">
<div class="signup-form">
<h2 class="form-title">Sign up</h2>
<form action="login.php" method="POST" class="register-form" id="register-form">
<!-- <div class="form-group">
<label for="name"><i class="zmdi zmdi-account material-icons-name"></i></label>
<input type="text" name="name" id="name" placeholder="Your Name" />
</div> -->
<div class="form-group">
<label for="email"><i class="zmdi zmdi-email"></i></label>
<input type="email" name="email" id="email" placeholder="Your Email" />
</div>
<div class="form-group">
<label for="pass"><i class="zmdi zmdi-lock"></i></label>
<input type="password" name="password" id="password" placeholder="Password" />
</div>


<div class="form-group form-button">
<input type="submit" name="login" id="login" class="form-submit" value="Login" />
</div>
</form>
</div>
<div class="signup-image">
<figure><img src="https://colorlib.com/etc/regform/colorlib-regform-7/images/signup-image.jpg" alt="sing up image"></figure>

</div>
</div>
</div>
</section>
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>
</html>