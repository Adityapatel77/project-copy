<?php
session_start();

if (!$_SESSION['email']) {

    header("Location: login.php"); //redirect to the login page to secure the welcome page without login access.  
}
// include_once("header.php");

?>
<!DOCTYPE html>
<html>

<head>
    <title>Add Product</title>
    <!-- 	 <link rel = "icon" href = "../project/image/img_tree.png" type = "image/x-icon"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper {
            width: 100%;
            margin: 0 auto;

        }

        .form-group {
            width: 50%;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Product</h2>
                    </div>
                    <form action="store_product.php" enctype="multipart/form-data" method="post">
                        <div class="form-group ">
                            <label>Name</label>
                            <input type="text" name="p_name" class="form-control" required>

                        </div>
                        <div class="form-group">
                            <label for=""> category </label>
                            <?php
                            include_once("connection/config.php");
                            $sql = "SELECT `orderId`, `Name` FROM category WHERE `status` = '1'";
                            $res = mysqli_query($link, $sql);
                            echo '<select class="form-control form-rounded" name="catName">';
                            foreach ($res as $key => $value) {
                                echo '<option value = ' . $value["orderId"] . '> ' . $value["Name"] . ' </option>';
                            }
                            echo '</select>';
                            ?>
                        </div>
                        <div class="form-group ">
                            <label>Product Code</label>
                            <input type="text" name="p_code" class="form-control"  value="<?php echo time()+mt_rand(10,5000);?>" disabled>
                        </div>

                        <div class="form-group ">
                            <label>Price</label>
                            <input type="text" name="price" class="form-control" >
                        </div>
                        <div class="form-group ">
                            <label>Quantity</label>
                            <input type="text" name="quantity" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label for="sel1">Order</label>
                            <select class="form-control" name = "order" id="sel1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Status :</label>
                            <input type="radio" id="active" name="status" value="1">
                            <label for="Active">Active</label>
                            <input type="radio" id="inactive" name="status" value="0">
                            <label for="inactive">Inactive</label><br>
                        </div>

                        <div class="form-group ">
                            <label>Image</label>
                            <input class="form-control form-control-lg" name="images[]" type="file"  multiple />

                        </div>


                        <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                        <a href="Product_index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
