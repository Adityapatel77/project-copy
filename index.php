<?php
session_start();
// echo $_SESSION['email'];
if (!isset($_SESSION['email'])) {

  header("Location: login.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>Index Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="../project1/assets/css/nav.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
  .topnav a:hover {
    background-color: #4CAF50;
    color: white;
  }
</style>


<body>
  <div class="topnav" id="myTopnav">
    <a href="#home" class="">Home </a>
    <a href="../project1/Add_category.php">Add Category</a>
    <a href="../project1/Add_product.php">Add Product</a>
    <a href="../project1/index.php">View Category</a>
    <a href="../project1/Product_Index.php">View Product</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
      <a href="logout.php" style="float: right;">Logout </a>
      <h4 style="color: white;float:right;padding:5px;">Welcome <?php echo $_SESSION['email']; ?></h4><br>
      <i class="fa fa-bars"></i>
    </a>

  </div>
  <!-- <div>
  <?php if (isset($_SESSION['success_message']) && !empty($_SESSION['success_message'])) { ?>
    <div class="success-message" style="margin-bottom: 20px;font-size: 20px;color: green;"><?php echo $_SESSION['success_message']; ?></div>
  <?php
    unset($_SESSION['success_message']);
  }
  ?>
</div> -->
  <div>
    <div class="col">
      <select class="btn btn-outline-success" style="float:right; padding-top:30px;" name="status" id="status">
        <option selected>all</option>
        <option value="1">Active</option>
        <option value="0">Inactive</option>
      </select>
    </div>
    <div class="form-group" style="width: 25%;">
      <label for="search">Search :</label>
      <input type="text" class="form-control" id="searchcat" name="searchcat" autofocus placeholder="Search">

    </div>

    <div>
      <?php
      // Include config file
      require_once("connection/config.php");

      // Attempt select query execution
      $sql = "SELECT * FROM category";
      if ($result = mysqli_query($link, $sql)) {
        if (mysqli_num_rows($result) > 0) {
          echo "<table class='table table-bordered table-striped' id='mylist'>";
          echo "<thead>";
          echo "<tr>";
          echo "<th>Id</th>";
          echo "<th>Name</th>";
          echo "<th>Order</th>";
          echo "<th>No Of Product</th>";
          echo "<th>Status</th>";
          echo "<th>Image</th>";
          echo "<th>Created At</th>";
          echo "<th>Updated At</th>";
          echo "<th>Action</th>";
          echo "</tr>";
          echo "</thead>";
          echo "<tbody id='data_table'>";
          while ($row = mysqli_fetch_array($result)) {
            $id  = $row['orderId'];
            $noOfProduct = "SELECT COUNT(p_id) AS noOfProduct FROM products 
            WHERE p_status = '1' AND orderId = $id";
            $resOfProduct = mysqli_query($link,$noOfProduct) or die("not find the no of product");      
            $rowOfProduct = mysqli_fetch_assoc($resOfProduct);

            echo "<tr>";
            echo "<td>" . $row['orderId'] . "</td>";
            echo "<td>" . $row['Name'] . "</td>";
            echo "<td>" . $row['order'] . "</td>";
            echo "<td>" . $rowOfProduct['noOfProduct'] . "</td>";

      ?>

            <td><?php if ($row['status'] == 1) {
                  echo '<button class="btn btn-success" onclick = ActiveInActive(' . $row["orderId"] . '); >Active</button>';
                } else {
                  echo '<button class="btn btn-danger" onclick = ActiveInActive(' . $row["orderId"] . ');>InActive</button>';
                } ?></td>

            <td><img src="images/<?php echo $row['image'] ?>" width="100px" height="100px"></td>
      <?php
            echo "<td>" . $row['createdat'] . "</td>";
            echo "<td>" . $row['updatedat'] . "</td>";
            echo "<td width= 150px>";

            echo "<a href='edit_category.php?id=" . $row['orderId'] . "' title='Update Record' data-toggle='tooltip' ><span class='glyphicon glyphicon-pencil'></span></a>";
            echo "<a href='delete_category.php?id=" . $row['orderId'] . "' title='Delete Record' data-toggle='tooltip'style='padding:10px'><span class='glyphicon glyphicon-trash'></span></a>";
            echo "</td>";
            echo "</tr>";
          }
          echo "</tbody>";
          echo "</table>";
        } else {
          echo "<script>";
          echo "alert('No any record please add record')";
          echo "</script>";
        }
      } else {
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
      }

      // Close connection
      mysqli_close($link);
      ?>

    </div>

</body>
<script src="js/validation.js"></script>
<script>
  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  function ActiveInActive(id) {
    var id = id;
    $.ajax({
      type: "POST",
      url: "status.php",
      data: {
        id: id
      },
      success: function(value) {
        $("#data_table").html(value);
      }

    });

  }
</script>

</html>