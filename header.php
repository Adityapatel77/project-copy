<?php
session_start();
// echo $_SESSION['email'];
if (!isset($_SESSION['email'])) {

  header("Location: login.php");
}

?>

<!DOCTYPE html>
<html>

<head>
  <title>Index Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="../project1/assets/css/nav.css" rel="stylesheet" type="text/css" media="all" />
</head>
<style type="text/css">
  .topnav a:hover {
    background-color: #4CAF50;
    color: white;
  }
</style>


<body>
  <div class="topnav" id="myTopnav">
    <a href="../project1/index.php" >Home </a>
    <a href="../project1/Add_category.php">Add Category</a>
    <a href="../project1/Add_product.php">Add Product</a>
    <a href="../project1/index.php">View Category</a>
    <a href="../project1/Product_Index.php">View Product</a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <a href="logout.php" style="float: right;">Logout </a>
    <h4 style="color: white;float:right;padding:5px;">Welcome <?php echo $_SESSION['email']; ?></h4><br>
      <i class="fa fa-bars"></i>
    </a>
    
  </div>
  </body>
  <script src="js/validation.js"></script>
<script>
  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
</script>
</html>